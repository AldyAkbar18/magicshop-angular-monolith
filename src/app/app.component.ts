import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './modules/authentication/authentication.service';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private debug = true;

  constructor(private authService: AuthenticationService) {

  }

  ngOnInit() {
    if (!environment.production && this.debug) {
      this.authService.login({username: 'Manuel', password: ''});
    }
  }
}
