import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ArticleAdministrationComponent} from './article-administration.component';
import {ArticleAdministrationRoutingModule} from './article-administration-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [ArticleAdministrationComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    ArticleAdministrationRoutingModule,
    ReactiveFormsModule
  ]
})
export class ArticleAdministrationModule {
}
