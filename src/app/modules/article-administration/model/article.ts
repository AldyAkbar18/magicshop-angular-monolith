export interface Article {
  id: number;
  name: string;
  price: string;
  active: boolean;
}
