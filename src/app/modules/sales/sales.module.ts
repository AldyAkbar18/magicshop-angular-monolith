import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SalesComponent} from './sales.component';
import {SalesRoutingModule} from './sales-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [SalesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    SalesRoutingModule,
    ReactiveFormsModule
  ]
})
export class SalesModule {
}
