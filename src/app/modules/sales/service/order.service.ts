import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Order } from "../model/order";

@Injectable({
  providedIn: "root",
})
export class OrderService {
  private static readonly ORDER_ENDPOINT = environment.apiUrl + "/sales";

  constructor(private http: HttpClient) {}

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(OrderService.ORDER_ENDPOINT);
  }
}
