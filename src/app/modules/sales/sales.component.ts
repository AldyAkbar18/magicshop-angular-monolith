import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Order} from './model/order';
import {OrderService} from './service/order.service';

@Component({
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {
  orders: Order[];
  activeOrder: Order;
  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.orderService.getOrders().subscribe(a => {
      this.orders = a;
      this.activeOrder = this.orders[0];
    });
    this.form = this.formBuilder.group({
      id: [0],
      articleId: [0],
      quantity: [0],
      completed: [false]
    });
  }

  onRowClick(rowIndex: number) {
    this.setActive(rowIndex);
  }

  onSave() {
    alert('Save data was clicked.');
  }

  private setActive(rowIndex: number) {
    this.activeOrder = this.orders[rowIndex];
  }
}
