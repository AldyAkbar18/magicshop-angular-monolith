import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {FlexModule} from '@angular/flex-layout';
import {UserPanelComponent} from './user-panel/user-panel.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    LoginComponent,
    UserPanelComponent
  ],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    FlexModule,
    ReactiveFormsModule
  ],
  exports: [
    UserPanelComponent
  ]
})
export class AuthenticationModule {
}
