import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

export interface User {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private isAuthenticatedSubj: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public isAuthenticated: Observable<boolean> = this.isAuthenticatedSubj.asObservable();
  private currentUserSubj: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public currentUser: Observable<User> = this.currentUserSubj.asObservable();

  constructor() {
  }

  login(user: User) {
    this.currentUserSubj.next(user);
    this.isAuthenticatedSubj.next(true);
  }

  logout() {
    this.currentUserSubj.next(null);
    this.isAuthenticatedSubj.next(false);
  }
}
