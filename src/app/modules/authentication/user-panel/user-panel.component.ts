import {Component, OnInit} from '@angular/core';
import {AuthenticationService, User} from '../authentication.service';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit {
  currentUser: User;

  constructor(public authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.authenticationService.currentUser.subscribe(u => this.currentUser = u);
  }

  onLogout() {
    this.authenticationService.logout();
  }
}
