import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationComponent} from './navigation.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule} from '@angular/router';
import {AuthenticationModule} from '../authentication/authentication.module';

@NgModule({
  declarations: [NavigationComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    AuthenticationModule
  ],
  exports: [
    NavigationComponent
  ]
})
export class NavigationModule { }
