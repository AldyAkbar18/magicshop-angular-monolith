import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication/authentication.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  navItems = [
    {
      name: 'Dashboard',
      link: '/dashboard'
    },
    {
      name: 'Article Administration',
      link: '/articleAdministration'
    },
    {
      name: 'Sales',
      link: '/sales'
    }
  ];

  constructor(public authService: AuthenticationService) {
  }

  ngOnInit(): void {
  }

}
