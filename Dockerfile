FROM node:10-alpine as build

WORKDIR /app
COPY package*.json /app/

RUN npm install

COPY . /app

ARG configuration=production

RUN cd /app && npm run ng build -- --output-path=./dist/out --configuration $configuration


FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*

COPY --from=build /app/dist/out /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
